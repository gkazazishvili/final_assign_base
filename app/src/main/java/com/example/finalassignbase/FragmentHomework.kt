package com.example.finalassignbase

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.finalassignbase.databinding.FragmentHomeworkBinding


class FragmentHomework() : BaseFragment<FragmentHomeworkBinding, PostsViewModel>(FragmentHomeworkBinding::inflate) {


    override fun getViewModel() = PostsViewModel::class.java

//    override var useSharedViewModel = true
//
//    override fun getViewModelClass(): Class<PostsViewModel> = PostsViewModel::class.java


    override fun start() {
        fetchData()
    }

    private fun fetchData() {

        viewModel.parseJson()
        viewModel.parsedJson.observe(viewLifecycleOwner, {

            binding.recyclerView.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            binding.recyclerView.adapter = Adapter(viewModel.parsedJson, ::registerBtn)

        })
    }



    private fun registerBtn():Button{
        return binding.registerBtn
    }


}






