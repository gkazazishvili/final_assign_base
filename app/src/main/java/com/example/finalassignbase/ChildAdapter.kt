package com.example.finalassignbase


import android.graphics.Color
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.graphics.red
import androidx.recyclerview.widget.RecyclerView
import com.example.finalassignbase.databinding.FragmentHomeworkBinding
import com.example.finalassignbase.databinding.ItemRowChildBinding
import com.example.finalassignbase.databinding.LayoutListItemBinding
import com.example.finalassignbase.databinding.SpinnerBinding
import com.google.android.material.textfield.TextInputLayout
import java.util.logging.Handler


class ChildAdapter(var list: FieldsDataSubList?, var handler: () -> Button) :
    RecyclerView.Adapter<ChildAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            SpinnerBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = list?.get(position)
        holder.bind(model!!)

    }

    override fun getItemCount(): Int {
        return list?.size!!
    }

    inner class ViewHolder(private val binding: SpinnerBinding) :
        RecyclerView.ViewHolder(binding.root) {


        fun bind(model: FieldsDataSubListItem) {


        binding.etInfo.hint = model.hint
        if (model.keyboard == EnumFields.TEXT.type)
        {
            binding.etInfo.inputType = InputType.TYPE_CLASS_TEXT
        } else if (model.keyboard == EnumFields.NUMBER.type)
        {
            binding.etInfo.inputType = InputType.TYPE_CLASS_NUMBER
        }

        if (model.field_type == EnumFields.INPUT.type)
        {
            binding.etInfo.inputType = InputType.TYPE_TEXT_VARIATION_WEB_EDIT_TEXT
        } else if (model.field_type == EnumFields.CHOOSER.type)
        {

            binding.tilInfo.endIconMode = TextInputLayout.END_ICON_DROPDOWN_MENU


        }
            if(model.required.toString() == EnumFields.REQUIRED.type){
                binding.etInfo.setBackgroundColor(Color.RED)
            }
            validateGenderBirthday(model)
            listener()


    }

        fun listener(){
            handler().setOnClickListener {
                Toast.makeText(handler().context, "Fill all your fields", Toast.LENGTH_SHORT).show()
            }
        }


        private fun validateGenderBirthday(model: FieldsDataSubListItem) {
            if (model.hint == EnumFields.GENDER.type) {
                val gender = listOf("Male", "Female")
                val adapter: ArrayAdapter<String?> = ArrayAdapter(
                    binding.root.context,
                    R.layout.layout_list_item, gender
                )
                (binding.etInfo).setAdapter(adapter)

            }

            if (model.hint == EnumFields.BIRTHDAY.type) {
                val birthday = listOf(1980, 1985, 1993, 1997, 2000)
                val adapter: ArrayAdapter<Int?> = ArrayAdapter(
                    binding.root.context,
                    R.layout.layout_list_item, birthday
                )
                (binding.etInfo).setAdapter(adapter)

            }
        }


    }

        }



