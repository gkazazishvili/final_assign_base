package com.example.finalassignbase

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding

typealias inflate<T> = (LayoutInflater, ViewGroup, Boolean) -> T
abstract class BaseFragment <VB : ViewBinding, VM : ViewModel>(private val inflate: inflate<VB>): Fragment(){



    open var useSharedViewModel: Boolean = false

    lateinit var viewModel : VM
    abstract fun getViewModel(): Class<VM>

//    protected lateinit var viewModel: ViewModel
//    protected abstract fun getViewModelClass(): Class<VM>

    private var _binding:VB? = null
    val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = inflate.invoke(inflater, container!!, false)
        viewModel = ViewModelProvider(requireActivity())[getViewModel()]
        start()
        return _binding!!.root
    }

//    abstract fun getViewModel() : Class<VM>


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    abstract fun start()
}