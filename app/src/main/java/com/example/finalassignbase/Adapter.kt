package com.example.finalassignbase

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class Adapter(private val list: LiveData<FieldsData>, var handler: () -> Button) :
    RecyclerView.Adapter<Adapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_row_parent, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {


        holder.recyclerView.layoutManager = LinearLayoutManager(holder.recyclerView.context)
        holder.recyclerView.adapter = ChildAdapter(list.value?.get(position), handler)
    }

    override fun getItemCount(): Int {
        return list.value?.size!!
    }

  class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

      var recyclerView: RecyclerView = itemView.findViewById(R.id.second_recyclerview)



    }



}


